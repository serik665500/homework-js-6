function createNewUser () {
  const userName = prompt ('What your name??');
  const userLastName = prompt ('What your LastName??');
  const userBirthDay = prompt("Input Your BirthDay 'dd.mm.yyyy'");
  const parts = userBirthDay.split('.');
  const getDate = new Date(parts[2], parts[1] -1, parts[0]);
  const newUser = {
    firstName: userName,
    lastName: userLastName,
    birthday: getDate,
    getAge() {
      const rightNow = new Date();
      const res = rightNow.getFullYear() - getDate.getFullYear();
      return res;
    },
    getLogin() {
      const lowerUserName = newUser.firstName[0].toLowerCase();
      const lowerLastName = newUser.lastName.toLowerCase();
      return lowerUserName + lowerLastName;
    },
    getPassword() {
      return newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + getDate.getFullYear();
    }
  }
  return newUser;
}

const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

